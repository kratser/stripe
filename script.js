const tabSwitchers = document.querySelectorAll("p[data-id='switcher']");
const tabInfoBlocks = document.querySelectorAll("div[data-id='tab']");
const closeTabBtn = document.querySelector("button[data-id='close-tab']")
function onTab() {
    tabSwitchers.forEach((item, inx) => {
        // const tab = item.querySelector(".tab");
        item.addEventListener("click", function() {
          closeTabBtn.classList.add("active")
          const nextSibling =  this.parentElement.nextElementSibling;
          const prevSibling = this.parentElement.previousElementSibling;
          if(nextSibling == null) {
             prevSibling.querySelector(".tab").classList.remove("active")
          }else {
            nextSibling.querySelector(".tab").classList.remove("active")
          }
            tabInfoBlocks.forEach(item => item.classList.remove("active"))
            this.classList.add("active");
            tabInfoBlocks[inx].classList.add("active")
        })
    })
}   
onTab();

function onCloseTab() {
  closeTabBtn.addEventListener("click", function(){
    closeTabBtn.classList.remove("active");
    tabSwitchers.forEach(item => item.classList.remove("active"))
    tabInfoBlocks.forEach(item => item.classList.remove("active"))
  })

}
onCloseTab()